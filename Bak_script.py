# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 17:33:13 2019

@author: Gabriel
"""

import numpy as np
import matplotlib.pyplot as plt
import random
from Bak import ci, iteration, bak1d, altura

N = 10
zc = 2
x, z_i = ci(N)

v = z_i.copy()

"""
for i in range(200):
    v = iteration(v,zc)
    print(v)

"""

v = bak1d(v, zc, N, z_i, z_max=10, plot=True, pause=.01)

print(v)

