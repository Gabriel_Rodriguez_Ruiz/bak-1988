# -*- coding: utf-8 -*-
"""
Created on Fri Jun 14 16:23:43 2019

@author: Gabriel
"""

import numpy as np
import matplotlib.pyplot as plt
import random
import time


def ci(N, z_max=10):
    """
    Function that returns the initial condition.
    
    Parameters
    ----------
    
    N : int
        Scale of the problem.
    z_max : int (optional)
        Maximum possible height.
    
    Returns
    -------
    
    x : array
        X coordinate.
    z_i : array
        Height differences.
    """
    x = np.linspace(0,N, N)
    z_i = np.zeros(N)
    for i in range(1,N):
        z_i[0] = 0
        r = random.randint(0, z_max)
        z_i[i] = r
    return x,z_i

def iteration(w,zc):
    """
    Function that makes an iteration.
    
    Parameters
    ----------

    w : array
        Array of heights before iteration.
    zc : array
        Critical height.
        
    Returns
    -------
    
    v : array
        Height differences after iteration.
    """
    v = w.copy()
    N = len(v)
    m = max(v)
    index = []
    for i in range(1, N-1):       
        if v[i]>zc:
            index.append(i)
            v[i] = v[i]-2
    if v[N-1]>zc:
        v[N-1] = v[N-1]-1
        index.append(N-1)
    for i in index:
        if i==(N-1):
            v[N-2] += 1
        else:
            v[i+1] += 1
            v[i-1] += 1
    v[0] = 0
    return v

def plot1d(v, N):
    """
    Plot of th 1D sand model.
    
    Parameters
    ----------
    
    v : array
        Height differences.
    N : int
        Horizontal length of the problem.
    """
#    plt.figure("Alturas 1D", clear=True)
    plt.xlabel("x")
    plt.ylabel("Altura")
    h = np.zeros(N)
    h[N-1] = v[N-1]
    x = np.linspace(0, N, N)
    for i in range(1,N):
        h[N-1-i] = h[N-i] + v[N-1-i]
    plt.step(x, h)
    plt.show()
    
def altura(v):
    """
    Returns the height for the 1D case from the height differences.
    
    Parameters
    ----------
    v : array
        Height differences.
    
    Returns
    -------
    h : array
        Height.
    """
    N = len(v)
    h = np.zeros(N)
    h[N-1] = v[N-1]
    for i in range(1,N):
        h[N-1-i] = h[N-i] + v[N-1-i]
    return h

def bak1d(v, zc, N, z_i, z_max=10, plot=True, pause=0.5):
    """
    The 1D sand fall model by Bak.
    
    Parameters
    ----------
    
    v : array
        Height differences.
    zc : array
        Critical height.
    N : int
        Horizontal length of the problem.
    pause : float (optional)
        Time pause in seconds.
    """

    v_original = np.zeros(N)
    while not np.array_equal(v, v_original):
        v_original = v
        v = iteration(v, zc)
        if plot:
            x = np.linspace(0,N, N)
            plt.step(x, altura(z_i))
            plot1d(v, N)
            plt.pause(pause)
            plt.figure("Alturas 1D", clear=True)
#    v[random.randint(1,N-1)]=v[random.randint(1,N-1)]+1
    return v
    