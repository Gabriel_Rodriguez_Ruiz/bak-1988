# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 16:31:32 2019

@author: Gabriel
"""


import numpy as np
import random
import matplotlib.pyplot as plt
from Bak2D import initial_condition2D, \
iteration2D, plot2D, bak2D, perturbation_closed, \
plot_domain
import time

for p in range(1):   
    N = 50
    n = 20000
    #n = 1000 tarda 35s
    zc = 5
    
    start = time.time()
    z_i = initial_condition2D(N,zc)
    
    z, i, zc_quantity, s= bak2D(z_i, zc, plot=False, slide=False)
    
    
    
    S = []      #sand sliding size
    I = []      #steps until equilibrium
    R = []      #position where the sand grain has been deposited
    D = []      #domain for each sand slide
    
    for k in range(n):
        z, i, s, r, domain = perturbation_closed(z, zc)
        S.append(s)
        I.append(i)
        R.append(r)
        D.append(domain)
    
    """
    #Domain plot
    for i in range(n):
        plot_domain(D[i])
        plt.pause(1)
        plt.clf()
    """
    
    end = time.time()
    elapsed = end-start

    #Guardado de datos
    timestr = time.strftime("%d%m%Y-%H%M%S")
    np.savez("{}".format(timestr), S=S, I=I, R=R, D=D, n=n, N=N)
    
    print(p)