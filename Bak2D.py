# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 17:24:20 2019

@author: Gabriel
"""
import numpy as np
import random
import matplotlib.pyplot as plt

def initial_condition2D(N, zc, condition="closed", z_max=10):
    """
    Function that returns the initial condition.
    
    Parameters
    ----------
    
    N : int
        Scale of the problem.
    condition : string (optional)
        Boundary condition.
    zc : int
        Critical height.
    z_max : int
        Maximum height.
    Returns
    -------
    
    x : array
        X coordinate.
    z_i : array
        Initial height differences.
    """
    x = np.linspace(0,N+1, N+2)
    y = np.linspace(0, N+1, N+2)
    xx, yy = np.meshgrid(x, y)
    z_i = np.zeros((N+2,N+2))
    if condition=="closed":
        z_i[0, :] = 0       #z(N+1,x)=0
        z_i[:, 0] = 0       #z(0,y)=0
        z_i[N+1, :] = 0       #z(x,N+1)=0
        z_i[:, N+1] = 0        #z(N+1,y)=0
    for i in range(1, N+1):
        for j in range(1, N+1):
            r = random.randint(0, z_max)
            z_i[i,j] = zc + r 
    return z_i

def plot2D(m):
    plt.imshow(m)
    plt.colorbar()
    
def iteration2D(m,zc):
    """
    Function that makes an iteration.
    
    Parameters
    ----------

    m : array
        Matrix of heights before iteration.
    zc : array
        Critical height.
  
    Returns
    -------
    
    z : array
        Height differences after iteration.
    zc_quantity : float
        Number of points with z>zc before iteration. 
    index : list
        Position of the cells with z>zc      
    """
    index = []
    N = len(m)-2      #escala del problema
    z = m.copy()
    zc_quantity = 0
    for i in range(1, N+1):
        for j in range(1, N+1):
            if z[i,j]>zc:
                z[i,j]-=4
                zc_quantity += 1
                index.append([i,j])
    for i,j in index:
        z[i+1,j]+=1
        z[i-1,j]+=1
        z[i,j+1]+=1
        z[i,j-1]+=1
    z[0,:] = 0
    z[:, 0] = 0       
    z[N+1, :] = 0       
    z[:, N+1] = 0
    return z, zc_quantity, index
                
def bak2D(z_i, zc, z_max=10, plot=True, pause=0.5, slide=False):
    """
    The 2D sand fall model by Bak.
    
    Parameters
    ----------
    
    z_i : array
        Initial matrix with height differences.
    zc : array
        Critical height.
    plot : bool(optional)
        True for plotting.
    pause : float (optional)
        Time pause in seconds.
    slide : bool (optional)
        To calculate or not the domain of the sand slide.
    Return
    ------
    
    z : array
        Height differences in the diagonal direction in equilibrium.
    i : int
        Number of iterations until equilibrium.
    zc_quantity_total : int
        Number of points with z>zc until equilibrium.
    domain : list
        List of lists with the position of the sand slide.
    """ 
    z = z_i
    z_loop = np.zeros_like(z_i)
    i = -1
    zc_quantity_total = 0
    domain = []
    while not np.array_equal(z,z_loop):
        z_loop = z.copy()
        z, zc_quantity_partial, index_before = iteration2D(z, zc)
        if slide==True:
            domain = sliding_position(domain, index_before)
        i += 1
        zc_quantity_total += zc_quantity_partial
        if plot==True:
            plt.figure("2D", clear=True)
            plot2D(z)
            plt.pause(pause)
    return z, i, zc_quantity_total, domain

def perturbation_closed(m, zc):
    """
    This function selects randomly a minimal stable site and induces a sand slide.
    
    Parameters
    ----------
    
    m : array
        Matrix of the system.
    zc : int
        Critical difference of heights.
    
    Returns
    -------
    
    z : array
        Final state after perturbation.
    i : float
        Total number of iterations until equilibrium.
    s : int
        Size of the sand slide.
    r : list
        Position of the grain deposition.
    domain : list
        Domain of the sand slide.
    """
    N = len(m)-2
    z = m.copy()
    index = []
    for i in range(1, N+1):
        for j in range(1, N+1):
            if z[i, j]==zc:
                index.append([i, j])
    r = random.choice(index)
    z[r[0], r[1]] += 1       #depositamos un grano de arena para iniciar una avalancha
    z, i, zc_quantity_total, domain = bak2D(z, zc, plot=False, slide=True)
    s = len(domain)
    return z, i, s, r, domain

def sliding_position(domain, index):
    """
    Function that saves the position of the sliding.
    
    Parameters
    ----------
    domain : list
        The cluster domain until iteration.
    index : list
        A list of lists with the position where z>zc before iteration.
    Returns
    -------
    domain : list
        A list of lists with the position of the sliding.
    """
    cluster_partial = index.copy()
    for i,j in index:
        x = []
        x.append([i+1, j])
        x.append([i-1, j])
        x.append([i, j+1])
        x.append([i, j-1])
        for k in x:
            if k not in cluster_partial:
                cluster_partial.append(k)
    for m in cluster_partial:
        if m not in domain:
            domain.append(m)   
    if []==domain[0]:
            domain.remove([])
    return domain
            
def plot_domain(data):
    data = np.array(data)
    N = data.max() + 5
    
    # color the background white (1 is white)
    arr = np.ones((N,N), dtype = 'bool')
    # color the dots black (0)
    arr[data[:,1], data[:,0]] = 0
    
    fig = plt.figure("Dominios")
    ax = fig.add_subplot(1, 1, 1)
    
    ax.imshow(arr, interpolation='nearest', cmap = 'gray')

    ax.invert_yaxis()
    # ax.axis('off')
    plt.show()